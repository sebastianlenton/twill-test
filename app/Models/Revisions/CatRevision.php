<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class CatRevision extends Revision
{
    protected $table = "cat_revisions";
}
